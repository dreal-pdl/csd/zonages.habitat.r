## code to prepare `tlv` dataset goes here
library(dplyr)

tlv <- datalibaba::importer_data(table = "r_tlv_000", schema = "sial_zonages_habitat", db = "production") %>%
  select(-reg_sel, -DEP, -NOM_DEPCOM) %>%
  mutate(TLV_lib = as.factor(TLV_lib))

usethis::use_data(tlv, overwrite = TRUE)
