#' Table communale nationale du zonage d application de l article 55 la loi SRU
#'
#' Table communale nationale du zonage d application de l article 55 la loi SRU. Les champs `population_municipale` et `unite_urbaine` expliquent l'entrée ou la sortie d'une commune dans le dispositif.
#'
#' @format A data frame with 34965 rows and 5 variables:
#' \describe{
  #'   \item{ DEPCOM }{  Code INSEE de la commune, COG 2023 }
  #'   \item{ art55_concer }{  Booléen indiquant si l'article 55 de la loi SRU concerne la commune }
  #'   \item{ art55_soum }{  Booléen indiquant si la commune est soumise à l'obligation art 55 loi SRU (quotat non atteint...) }
  #'   \item{ pop_municipale }{  Population mucicipale INSEE (fichier des populations légales 2020) }
  #'   \item{ unite_urbaine }{  Information de synthèse sur les unités urbaines 2020 : on indique le nom de l'UU si elle contient plus de 50000 habitants, d'après la tranche d unité urbaine calculée sur la population 2017. }
  #' }
  #' @source suivi DGALN pour les champs SRU, INSEE pour la population municipale et les unités urbaines 2020 (https://www.insee.fr/fr/information/2028028)
  "sru"


