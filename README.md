
<!-- README.md is generated from README.Rmd. Please edit that file -->

# zonages.habitat.r

<!-- badges: start -->

[![pipeline
status](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/zonages.habitat.r/badges/master/pipeline.svg)](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/zonages.habitat.r/-/commits/master)
<!-- badges: end -->

L’objectif du package R `{zonages.habitat.r}` est de diffuser le
rattachement de chaque commune aux zonages d’application des politiques
publiques du logement en tenant compte des évolutions du contour des
communes :

-   zonage ABC (`abc`),  
-   délégataires des aides à la pierre (`delegataires`),  
-   zonage HLM123 (`hlm123`),  
-   communes SRU (`sru`),
-   communes ayant instauré une taxe sur les logements vacants (`tlv`).

## Installation

Il s’installe depuis la forge gitlab du ministère de l’écologie.

``` r
remotes::install_gitlab(repo = "dreal-pdl/csd/zonages.habitat.r", host = "gitlab-forge.din.developpement-durable.gouv.fr")
```

## Exemple

Voici par exemple un aperçu des données du zonage ABC :

``` r
library(zonages.habitat.r)
head(abc)
#>   DEPCOM zon_abc
#> 1  01001       C
#> 2  01002       C
#> 3  01004      B2
#> 4  01005       C
#> 5  01006       C
#> 6  01007       C
```

## Présentation du package

Lien vers le site pkgdown de présentation du package :  
<https://dreal-pdl.gitlab-pages.din.developpement-durable.gouv.fr/csd/zonages.habitat.r/articles/a_package_zonages_habitat.html>

## Signalement de bugs ou demandes d’amélioration

Les bugs ou les demandes d’amélioration sont les bienvenues :
<https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/zonages.habitat.r/-/issues>
