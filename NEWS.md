# zonages.habitat.r 0.0.2

* Actualisation du zonage ABC suite révision d'octobre 2023 (COG 2023).

# zonages.habitat.r 0.0.1

* Actualisation COG 2023 des données de zonage habitat.

# zonages.habitat.r 0.0.0.9000

* Added a `NEWS.md` file to track changes to the package.
